<?php
$page = 'homepage';
include 'header.php';
?>

<!-- Home Page Intro -->
<div class="home-page-intro">
    <div class="container">
        <div class="row">

            <div class="col-md-6">

                <h1 data-aos="flip-left" data-aos-delay="50">
                    Удвои свойте продажби сега!
                </h1>

                <div class="home-page-intro-register" data-aos="flip-left" data-aos-delay="40">
                    <a href="/startup.php" class="btn btn-pink">ЗАПОЧНИ БЕЗПЛАТНО!</a>
                    <a href="/startup.php" class="home-page-intro-see-how-we-do"><i class="fa fa-play-circle"></i>&nbsp; виж как става</a>
                </div>

                <div class="yes-its-free" data-aos="flip-left" data-aos-delay="50">
                    <p>Създай свой собствен онлайн магазин с помощта на Credo Cart</p>
                    <small>Ще ти отнеме само 5 минути.</small>
                </div>
            </div>

            <div class="col-md-6" data-aos="flip-left" data-aos-delay="50">
                <img src="img/home_side.jpg" class="img-responsive" />
            </div>

        </div>
    </div>
</div>
<!-- Home Page Intro -->



<!-- Separated Section -->
<div class="why-must-use padding-top50 padding-bottom50">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <h2 class="text-bold">Защо да използвам Credo Cart?</h2>
                <p>Защото имаме всичко от което се нуждаеш!</p>
            </div>

            <div class="padding-top30">
                <div class="col-sm-4">
                    <div class="left-content">
                        <div class="features"  data-aos="zoom-in" data-aos-delay="100">
                            <i class="fa fa-globe pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Мултиезичност </h4>
                                <p>можеш да продаваш продуктите си във всички страни</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="200">
                            <i class="fa fa-euro-sign pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Валутни курсове </h4>
                                <p>Автоматична синхронизация на валутните курсове</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                            <i class="fa fa-file-invoice-dollar pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Данъци </h4>
                                <p>можеш да начисляваш данъци за различните страни, градове или катеогории на продуктите</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="400">
                            <i class="fa fa-chart-pie pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Статистики </h4>
                                <p>пълни статистики за всичко от което се нуждаете да си направите отчет</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="right-content">
                        <div class="features" data-aos="zoom-in" data-aos-delay="100">
                            <i class="fa fa-download pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Автоматични обновления</h4>
                                <p>всеки ден ще получавате нашите обновления и новости</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="200">
                            <i class="fa fa-puzzle-piece pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Приложения</h4>
                                <p>набор от приложения с които ще подобрите и удвоите вашите продажби</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                            <i class="fa fa-tags pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Промо кодове</h4>
                                <p>създаване на промо кодове за вашите клиенти</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="400">
                            <i class="fa fa-users pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Потребителски роли</h4>
                                <p>Създаване на роли за администраторите</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="right-content">
                        <div class="features" data-aos="zoom-in" data-aos-delay="100">
                            <i class="fa fa-compass pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Менюта </h4>
                                <p>разширено управление на менютата в сайта</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="200">
                            <i class="fa fa-file-alt pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Страници </h4>
                                <p>създаване на напълно авторски страници</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                            <i class="fa fa-comments pull-left text-gradient"></i>
                            <div class="text">
                                <h4>Ревюта  </h4>
                                <p>ревюта на продуктите с коментар и оценка</p>
                            </div>
                        </div>
                        <div class="features" data-aos="zoom-in" data-aos-delay="400">
                            <i class="fa fa-phone pull-left text-gradient"></i>
                            <div class="text">
                                <h4>24/7 Поддръжка</h4>
                                <p>отзивчивост от нашия екип</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Separated Section ends here-->

<div class="gradient-background padding-top60 padding-bottom60">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center"  data-aos="flip-left" data-aos-delay="55">
                <h2 class="text-bold">Липсва функционалност? <br />Свържи се с нас за да я разработим.</h2>
                <div class="text-center"><a href="/contact.php" class="btn btn-outline-light btn-large btn-radius">Пиши ни</a></div>
                <div class="text-center padding-top10"> или се обади на <i class="fa fa-phone"></i> <b>088 545 1012</b></div>
            </div>
        </div>
    </div>
</div>

<!-- Boxes style one -->
<div class="padding-top50 padding-bottom50">
    <div class="container"  data-aos="flip-left" data-aos-delay="55">
        <div class="text-center">
            <h2 class="text-bold">Имате магазин, който е на друга платформа?</h2>
            <h2 class="text-bold">Няма проблем!</h2>
            <div class="padding-top10 text-white-blue text-17">
                <p>Можете да мигрирате своя магазин, запазвайки продуктите, поръчките и клиентите си с един клик. </p>
                <p>Разработили сме модули за най-популярните готови платформи от които можете да мигрирате магазина си бързо и лесно,</p>
                <p>без да загубите никаква информация.</p>
                <p>Ако имате нужда, ние ще Ви помогнем да мигрирате магазина си безпроблемно.</p>
            </div>
        </div>
        <div class="padding-top30">
            <div class="row">
                <div class="all-partners">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                        <div class="partner-logo">
                            <img src="migrations/opencart-logo.png" class="img-responsive img-center">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                        <div class="partner-logo">
                            <img src="migrations/woocommerce-logo.png" class="img-responsive img-center">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                        <div class="partner-logo">
                            <img src="migrations/shopify-logo.png" class="img-responsive img-center">
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4">
                        <div class="partner-logo">
                            <img src="migrations/cloudcart-logo.png" class="img-responsive img-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Boxes style one ends here-->



<!-- Testimonials slider -->
<section class="testimonials" id="carousel">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="4000">
                    <ol class="carousel-indicators">
                        <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">

                        <div class="item active">
                            <div class="col-sm-4">
                                <div class="text-left">
                                    <h2 style="margin:0px;">Какво казват</h2><h2 style="margin:0px;margin-bottom:15px;">клиентите за нас?</h2>
                                    <p>Това са част от отзивите на нашите клиенти. </p>
                                    <div class="buttons">
                                        <a href="#" class="btn btn-outline-green btn-large">Виж всички отзиви <i class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="profile-circle"><img src="img/other/testimonial1.jpg" class="img-responsive"
                                                                 alt="">
                                </div>
                                <blockquote>
                                    <p><i class="fa quote fa-quote-right fa-sm pull-left"></i>
                                        <br>
                                        Услугата е супер, всичко става бързо и админ панела е прост и работещ. <i class="fa quote fa-quote-right fa-sm "></i></p>
                                    <small>Ивайло Христов</small>
                                </blockquote>
                            </div>
                            <div class="col-sm-4">
                                <div class="profile-circle"><img src="img/other/testimonial2.jpg" class="img-responsive"
                                                                 alt="">
                                </div>
                                <blockquote>
                                    <p><i class="fa quote fa-quote-right fa-sm pull-left"></i>
                                        <br>
                                        Екипа е отзивчив, услугата е добра. Харесват ми поддръжката и дневните новости на софтуера.<i class="fa quote fa-quote-right fa-sm "></i></p>
                                    <small>Христина Илиева</small>
                                </blockquote>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--  Testimonials slider ends here -->


<!-- Partner Program -->
<div class="gradient-background padding-top40 padding-bottom50">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="text-bold">Партньорска програма</h2>
                <p>Печели до 50% от печалбата на всеки клиент който е станал наш клиент благодарение на теб!</p>
            </div>
            <div class="col-sm-6">
                <div class="text-right padding-top20">
                    <a href="#" class="btn btn-outline-light btn-large btn-block">Разбери повече <i class="fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Partner Program -->

<?php include 'footer.php'; ?>