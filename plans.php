<?php
include 'header.php';
?>

<section id="main-body">

    <div class="container" style="padding-top:15px;min-height:777px;">
        <div class="row">
            <!-- Container for main page display content -->
            <div class="col-xs-12 main-content">

                <div class="layout-text right-layout padding-bottom50 padding-top30">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 text-center">
                                <img src="img/buider-image.png" class="aos-init aos-animate" style="max-width:100%;" alt="" data-aos="fade-right">
                            </div>
                            <div class="col-md-6">
                                <div class="aos-init aos-animate" data-aos="fade-up">
                                    <h2 class="text-bold">Изгради свой собствен онлайн магазин с Credo Cart!</h2>


                                    <div class="text-content">
                                        <i class="far fa-check-circle pull-left"></i>
                                        <div class="text">
                                            <p>Без първоначална ивестиция</p>
                                        </div>
                                    </div>


                                    <div class="text-content">
                                        <i class="far fa-check-circle pull-left"></i>
                                        <div class="text">
                                            <p>Достъп до всички теми</p>
                                        </div>
                                    </div>


                                    <div class="text-content">
                                        <i class="far fa-check-circle pull-left"></i>
                                        <div class="text">
                                            <p>Достъп до всички модули</p>
                                        </div>
                                    </div>


                                    <div class="text-content">
                                        <i class="far fa-check-circle pull-left"></i>
                                        <div class="text">
                                            <p>Достъп до всички обновления</p>
                                        </div>
                                    </div>

                                    <div class="buttons">
                                        <p>Имаш повече въпроси относно стартирането на онлайн магазин?</p>
                                        <p>Свържи се с нас на тел. 088 545 1012 или ни пиши на info@credocart.bg</p>
                                        <a href="contact.php" class="btn btn-outline-green btn-large btn-block text-bold">Безплатна консултация <i class="fas fa-long-arrow-alt-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr />

                <div class="pricing-tables padding-top30 padding-bottom50">
                    <div class="container">
                        <div class="row">

                            <div class="text-center padding-bottom50">
                                <h2 class="text-bold">Нашите планове</h2>
                                <div class="padding-top10 text-white-blue text-17">
                                    <p>Ако сте намерили подобна услуга, която е по-евтина и качествена от тази която ние</p>
                                    <p>предлагаме, може да се свържете с нас да договорим нова цена на вашия план. </p>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="pricing-table">

                                    <div class="pricing-table-header">

                                        FREE!

                                    </div>

                                    <div class="pricing-table-content">
                                        <h4>Безплатен </h4>

                                        <p>(До първи оборот от 500лв.)</p>
                                        <br />
                                        <div class="table-list">
                                            <ul>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 100 продукта</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 2 имейл акаунта</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 10 GB дисково пространство</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Достъп до вички модули и теми</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SSL сертификат</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Неограничен трафик</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматична инсталация</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SEO интрументи</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматични актуализации</li>
                                            </ul>
                                        </div>

                                        <div class="buttons">
                                            <a href="https://members.credocart.bg/cart.php?a=add&pid=4" class="btn btn-outline-green btn-large btn-block text-bold" id="product1-order-button">
                                                Избери
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="pricing-table">

                                    <div class="pricing-table-header">

                                        от
                                        <span>9.99лв.</span> / <small>месец</small>
                                        <br>

                                    </div>

                                    <div class="pricing-table-content">
                                        <h4>Мини</h4>

                                        <p>Подходящ за стартиращи бизнеси</p>
                                        <br />
                                        <div class="table-list">
                                            <ul>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 350 продукта</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 5 имейл акаунта</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 15 GB дисково пространство</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Достъп до вички модули и теми</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SSL сертификат</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Неограничен трафик</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматична инсталация</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SEO интрументи</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматични актуализации</li>
                                            </ul>
                                        </div>

                                        <div class="buttons">
                                            <a href="https://members.credocart.bg/cart.php?a=add&pid=1" class="btn btn-outline-green btn-large btn-block text-bold" id="product2-order-button">
                                                Избери
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="pricing-table">

                                    <div class="pricing-table-header">

                                        <span>26.99лв.</span> / <small>месец</small>
                                        <br>

                                    </div>

                                    <div class="pricing-table-content">
                                        <h4>Среден</h4>

                                        <p>Подходящ за вече развити бизнеси</p>
                                        <br />
                                        <div class="table-list">
                                            <ul>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 650 продукта</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 15 имейл акаунта</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 35 GB дисково пространство</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Достъп до вички модули и теми</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SSL сертификат</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Неограничен трафик</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматична инсталация</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SEO интрументи</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматични актуализации</li>
                                            </ul>
                                        </div>

                                        <div class="buttons">
                                            <a href="https://members.credocart.bg/cart.php?a=add&pid=2" class="btn btn-outline-green btn-large btn-block text-bold" id="product3-order-button">
                                                Избери
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="pricing-table">

                                    <div class="pricing-table-header">

                                        <span>69.99лв.</span> / <small>месец</small>
                                        <br>

                                    </div>

                                    <div class="pricing-table-content">
                                        <h4>Професионален</h4>


                                        <p>Неограничен</p>
                                        <br />
                                        <div class="table-list">
                                            <ul>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Неограничен брой продукти</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 30 имейл акаунта</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> 100GB дисково пространство</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Достъп до вички модули и теми</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SSL сертификат</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Неограничен трафик</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматична инсталация</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> SEO интрументи</li>
                                                <li><i data-aos="zoom-in" class="fa fa-check aos-init aos-animate"></i> Автоматични актуализации</li>
                                            </ul>
                                        </div>

                                        <div class="buttons">
                                            <a href="https://members.credocart.bg/cart.php?a=add&pid=3" class="btn btn-outline-green btn-large btn-block text-bold" id="product4-order-button">
                                                Избери
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>


            </div><!-- /.main-content -->


            <div class="clearfix"></div>

        </div>

    </div>

</section>

<?php
include 'footer.php';
?>
