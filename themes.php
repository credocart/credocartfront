<?php include 'header.php'; ?>


<section id="main-body">


    <div class="container" style="padding-top:15px;min-height:777px;">
        <div class="row">
            <!-- Container for main page display content -->
            <div class="col-xs-12 main-content">
                <!-- <div class="header-lined">
<h1>Теми</h1>
<ol class="breadcrumb">
<li>
<a href="/index.php">            Начало
</a>        </li>
<li class="active">
            Теми
        </li>
</ol>
</div>
-->


                <div class="container">
                    <div class="row">

                        <div class="col-md-4 padding-top40">
                            <div class="row">
                                <img src="https://colorlib.com/wp/wp-content/uploads/sites/2/fashe-free-shopify-ecommerce-website-template.jpg" class="img-responsive" style="height:350px;" />

                                <div class="col-md-6">
                                    <h2 class="text-bold" style="padding:0px;margin:0px;">Fashe</h2>
                                </div>
                                <div class="col-md-6">
                                    <a href="https://fashe.credocart.bg" target="_new" class="btn btn-outline-green btn-large btn-block text-bold">Демо <i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                                <div class="col-md-6">
                                    <!-- <a href="https://colorlib.com" target="_new"><i class="fa fa-user"></i> ColorLib</a> -->
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 padding-top40">
                            <div class="row">
                                <img src="https://colorlib.com/wp/wp-content/uploads/sites/2/coloshop-free-bootstrap-ecommerce-website-template.jpg" class="img-responsive" style="height:350px;" />

                                <div class="col-md-6">
                                    <h2 class="text-bold" style="padding:0px;margin:0px;">ColoShop</h2>
                                </div>
                                <div class="col-md-6">
                                    <a href="https://coloshop.credocart.bg" target="_new" class="btn btn-outline-green btn-large btn-block text-bold">Демо <i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                                <div class="col-md-6">
                                    <!-- <a href="https://colorlib.com" target="_new"><i class="fa fa-user"></i> ColorLib</a> -->
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4 padding-top40">
                            <div class="row">
                                <img src="https://colorlib.com/wp/wp-content/uploads/sites/2/electro-free-ecommerce-template.jpg" class="img-responsive" style="height:350px;" />

                                <div class="col-md-6">
                                    <h2 class="text-bold" style="padding:0px;margin:0px;">Electro</h2>
                                </div>
                                <div class="col-md-6">
                                    <a href="https://electro.credocart.bg" target="_new" class="btn btn-outline-green btn-large btn-block text-bold">Демо <i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                                <div class="col-md-6">
                                    <!-- <a href="https://colorlib.com" target="_new"><i class="fa fa-user"></i> ColorLib</a> -->
                                </div>
                            </div>
                        </div>



                        <div class="col-md-4 padding-top40">
                            <div class="row">
                                <img src="https://colorlib.com/wp/wp-content/uploads/sites/2/vegefoods-free-template.jpg" class="img-responsive" style="height:350px;" />

                                <div class="col-md-6">
                                    <h2 class="text-bold" style="padding:0px;margin:0px;">VegeFoods</h2>
                                </div>
                                <div class="col-md-6">
                                    <a href="https://vegefoods.credocart.bg" target="_new" class="btn btn-outline-green btn-large btn-block text-bold">Демо <i class="fas fa-long-arrow-alt-right"></i></a>
                                </div>
                                <div class="col-md-6">
                                    <!-- <a href="https://colorlib.com" target="_new"><i class="fa fa-user"></i> ColorLib</a> -->
                                </div>
                            </div>
                        </div>




                    </div>
                </div>



            </div><!-- /.main-content -->


            <div class="clearfix"></div>

        </div>

    </div>

</section>


<?php include 'footer.php'; ?>