
<!-- Dark Footer -->
<footer class="dark-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4>Бързи връзки</h4>
                <ul>
                    <li><a href="startup.php">Регистрация на магазин</a></li>
                    <li><a href="plans.php">Месечни планове</a></li>
                    <li><a href="contact.php">Предложи нова функционалност</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>За клиенти</h4>
                <ul>
                    <li><a href="https://login.credocart.bg">Вход</a></li>
                    <li><a href="https://login.credocart.bg/register">Регистрация</a></li>
                    <li><a href="https://login.credocart.bg/password/reset">Забравена парола</a></li>
                    <li><a href="https://members.credocart.bg/">Контролен панел</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Контакти</h4>
                <div class="location-info">
                    <h5>София, Младост 4 Бизнес парк</h5>
                    <h5>&nbsp;</h5>
                    <h5><i class="fa fa-envelope text-gradient"></i> info@credocart.bg</h5>
                    <h5><i class="fa fa-phone text-gradient"></i> 088 545 1012</h5>
                </div>
            </div>
            <div class="col-sm-3">
                <h4>Социални мрежи</h4>
                <div class="social-media">
                    <a href="#"><i class="fab fa-facebook-f text-gradient"></i></a>
                    <a href="#"><i class="fab fa-google text-gradient"></i></a>
                    <a href="#"><i class="fab fa-linkedin-in text-gradient"></i></a>
                    <a href="#"><i class="fab fa-instagram text-gradient"></i></a>
                </div>
            </div>
            <div class="col-sm-12">

                <div class="footer-copyright">
                    <p class="pull-left"> &copy; 2017-2020 CredoCart.BG - Безплатен софтуер за онлайн магазин.</p>  <p class="pull-right">Всички права са запазени.</p>
                </div>

            </div>
        </div>
    </div>

</footer>
<!-- Daek Footer ends here -->

<div class="modal system-modal fade" id="modalAjax" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content panel panel-primary">
            <div class="modal-header panel-heading">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Title</h4>
            </div>
            <div class="modal-body panel-body">
                Loading...
            </div>
            <div class="modal-footer panel-footer">
                <div class="pull-left loader">
                    <i class="fas fa-circle-notch fa-spin"></i> Loading...
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary modal-submit">
                    Submit
                </button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="js/validator.js"></script>
<script type="text/javascript" src="js/contact.js"></script>
<script type="text/javascript" src="js/bootsnav.js"></script>
<script type="text/javascript" src="js/aos.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

</body>

</html>