<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CredoCart.BG - Безплатен софтуер за онлайн магазин.</title>
    <!-- Styling -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600|Comfortaa:400,700" rel="stylesheet">
    <link href="css/all.min.css?v=40cb1e" rel="stylesheet">
    <link href="css/fontawesome-all.min.css" rel="stylesheet">

    <link href="css/fontawesome-icons/css/all.min.css" rel="stylesheet">

    <link href="css/bootsnav.css" rel="stylesheet">
    <link href="css/aos.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">

    <link href="css/credocart.css?time=1579203267" rel="stylesheet">
    <link href="css/credocart-responsive.css?time=1579203267" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script src="js/scripts.min.js?v=40cb1e"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />

    <style>

        #primary-nav {
            padding-top:10px !important;
        }
        section#main-body {
            padding-top: 23px !important;
        }
        #frmCheckout {
            background: #fff;
            padding: 22px;
            border-radius: 9px;
            margin-top: 20px;
        }
        .product-details-tab-container {
            background:#fff;
        }
        .table-list {
            margin:0px;
        }
        .dataTables_info {
            border-radius:4px !important;
            padding-bottom:10px !important;
        }
        .dataTables_filter input.form-control {
            height:35px !important;
        }
    </style>
</head>

<body data-phone-cc-input="1">

<script src="//cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#f7f9ff",
                "text": "#5e58aa"
            },
            "button": {
                "background": "#08bd56",
                "text": "#FFF"
            }
        },
        "theme": "classic",
        "content": {
            "message": "CredoCart.com използва бисквитки (cookies). Използвайки този сайт, приемате нашите",
            "dismiss": "Разбрах!",
            "link": " Общи условия",
            "href": "https://credocart.bg/cookiepolicy.php"
        }
    });
</script>

<style>
    @media only screen and (max-width:994px) {
        .navbar-brand img {
            width:175px;
        }
    }
</style>

<?php
if (!isset($page)) {
    $page = false;
}
?>

<!-- Navbar content -->
<?php if ($page == 'home'): ?>
<nav class="navbar navbar-sticky navbar-transparent bootsnav">
<?php else: ?>
<nav class="navbar navbar-sticky navbar-default dark bootsnav">
<?php endif; ?>


    <div class="container">

        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand aligment-for-home" href="/">
                <span class="navbar-brand-text">Credo Cart</span>
                <!--	<img src="credo-cart-logo-1.png" style="margin-top:0px;" />-->
            </a>
        </div>
        <!-- End Header Navigation -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <div class="navbar-form navbar-right">

                <a href="https://login.credocart.bg" class="btn btn-outline-light">Вход</a>
            </div>
            <ul class="nav navbar-nav navbar-right" data-in="fadeIn" data-out="fadeOut">
                <li>
                    <a href="index.php" class="active">Начало</a>
                </li>
                <li>
                    <a href="themes.php">Теми</a>
                </li>
                <li>
                    <a href="modules.php">Модули</a>
                </li>
                <li>
                    <a href="clients.php">Клиенти</a>
                </li>
                <li>
                    <a href="plans.php">Цени</a>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->


    </div>

</nav>
<!-- Navbar Content ends here -->