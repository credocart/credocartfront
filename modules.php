<?php
include 'header.php';
?>



    <section id="main-body">


        <div class="container" style="padding-top:15px;min-height:777px;">
            <div class="row">
                <!-- Container for main page display content -->
                <div class="col-xs-12 main-content">
                    <!-- <div class="header-lined">
<h1>Модули</h1>
<ol class="breadcrumb">
    <li>
    <a href="/index.php">            Начало
    </a>        </li>
    <li class="active">
                Модули
            </li>
</ol>
</div>
-->




                    <!-- Separated Section -->
                    <div class="padding-bottom50">
                        <div class="container">
                            <div class="row">
                                <div class="text-center">
                                    <h2 class="text-bold">Методи за доставка</h2>
                                    <p>възползвай се от различни методи за доставка</p>
                                </div>

                                <div class="padding-top30">

                                    <div class="col-sm-3 text-center">
                                        <div class="features"  data-aos="zoom-in" data-aos-delay="100">
                                            <img src="img/econt.jpg" style="max-height:100px;width:225px;" />
                                        </div>
                                    </div>

                                    <div class="col-sm-3 text-center">
                                        <div class="features" data-aos="zoom-in" data-aos-delay="200">
                                            <img src="img/speedy.png" style="width:272px;max-height:100px;" />
                                        </div>
                                    </div>



                                    <div class="col-sm-3 text-center">
                                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                            <img src="img/leoexpress.svg" style="width:135px;height:100px;" />
                                        </div>
                                    </div>

                                    <div class="col-sm-3 text-center">
                                        <div class="features" data-aos="zoom-in" data-aos-delay="300" style="background:#d2232a;padding:15px;border-top-left-radius:50px;border-bottom-right-radius:50px;">
                                            <img src="img/rapido.jpg" style="width:235px;" />
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Separated Section ends here-->


                    <hr />

                    <!-- Separated Section -->
                    <div class="padding-bottom50">
                        <div class="container">
                            <div class="row">
                                <div class="text-center">
                                    <h2 class="text-bold">Разплащателни методи</h2>
                                    <p>възползвай се от различни методи за разплащане</p>
                                </div>

                                <div class="padding-top30">



                                    <div class="col-sm-3 text-center">
                                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                            <img src="https://craftindustryalliance.org/wp-content/uploads/2019/04/PayPal_logo_logotype_emblem.png" style="width:100%;max-height:100px;" />
                                        </div>
                                    </div>

                                    <div class="col-sm-3 text-center">
                                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                            <img src="https://cdn.pixabay.com/photo/2017/09/18/08/56/credit-card-2761073_960_720.png" style="max-height:100px;" />
                                        </div>
                                    </div>

                                    <div class="col-sm-3 text-center">
                                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                            <img src=" https://pngimage.net/wp-content/uploads/2018/05/bank-transfer-png-3.png" style="max-height:100px;" />
                                        </div>
                                    </div>

                                    <div class="col-sm-2 text-center">
                                        <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                            <img src="img/pay-on-dilevery.jpg" style="max-height:100px;" />
                                        </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Separated Section ends here-->


                    <hr />

                    <!-- Separated Section -->
                    <div class="padding-top20 padding-bottom40">
                        <div class="container">
                            <div class="row">
                                <div class="text-center">
                                    <h2 class="text-bold">Модули</h2>
                                    <p>Пълен пакет от модули нужни за твоя онлайн магазин</p>
                                </div>

                                <div class="padding-top30">
                                    <div class="col-sm-4">
                                        <div class="left-content">
                                            <div class="features"  data-aos="zoom-in" data-aos-delay="100">
                                                <i class="fa fa-globe pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Мултиезичност </h4>
                                                    <p>можеш да продаваш продуктите си във всички страни</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="200">
                                                <i class="fa fa-euro-sign pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Валутни курсове </h4>
                                                    <p>Автоматична синхронизация на валутните курсове</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                                <i class="fa fa-file-invoice-dollar pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Данъци </h4>
                                                    <p>можеш да начисляваш данъци за различните страни, градове или катеогории на продуктите</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="400">
                                                <i class="fa fa-chart-pie pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Статистики </h4>
                                                    <p>пълни статистики за всичко от което се нуждаете да си направите отчет</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="right-content">
                                            <div class="features" data-aos="zoom-in" data-aos-delay="100">
                                                <i class="fa fa-download pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Автоматични обновления</h4>
                                                    <p>всеки ден ще получавате нашите обновления и новости</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="200">
                                                <i class="fa fa-puzzle-piece pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Приложения</h4>
                                                    <p>набор от приложения с които ще подобрите и удвоите вашите продажби</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                                <i class="fa fa-tags pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Промо кодове</h4>
                                                    <p>създаване на промо кодове за вашите клиенти</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="400">
                                                <i class="fa fa-users pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Потребителски роли</h4>
                                                    <p>Създаване на роли за администраторите</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="right-content">
                                            <div class="features" data-aos="zoom-in" data-aos-delay="100">
                                                <i class="fa fa-compass pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Менюта </h4>
                                                    <p>разширено управление на менютата в сайта</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="200">
                                                <i class="fa fa-file-alt pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Страници </h4>
                                                    <p>създаване на напълно авторски страници</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="300">
                                                <i class="fa fa-comments pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>Ревюта  </h4>
                                                    <p>ревюта на продуктите с коментар и оценка</p>
                                                </div>
                                            </div>
                                            <div class="features" data-aos="zoom-in" data-aos-delay="400">
                                                <i class="fa fa-phone pull-left text-gradient"></i>
                                                <div class="text">
                                                    <h4>24/7 Поддръжка</h4>
                                                    <p>отзивчивост от нашия екип</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Separated Section ends here-->




                </div><!-- /.main-content -->


                <div class="clearfix"></div>

            </div>

        </div>

    </section>


<?php
include 'footer.php';
?>